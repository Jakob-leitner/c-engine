var annotated_dup =
[
    [ "MathUtil", "namespace_math_util.html", [
      [ "TrigonometricTraits", "struct_math_util_1_1_trigonometric_traits.html", null ],
      [ "TrigonometricTraits< double >", "struct_math_util_1_1_trigonometric_traits_3_01double_01_4.html", "struct_math_util_1_1_trigonometric_traits_3_01double_01_4" ],
      [ "TrigonometricTraits< float >", "struct_math_util_1_1_trigonometric_traits_3_01float_01_4.html", "struct_math_util_1_1_trigonometric_traits_3_01float_01_4" ],
      [ "TrigonometricTraits< long double >", "struct_math_util_1_1_trigonometric_traits_3_01long_01double_01_4.html", "struct_math_util_1_1_trigonometric_traits_3_01long_01double_01_4" ]
    ] ],
    [ "mmt_gd", "namespacemmt__gd.html", [
      [ "BulletPool", "classmmt__gd_1_1_bullet_pool.html", "classmmt__gd_1_1_bullet_pool" ],
      [ "CameraRenderComponent", "classmmt__gd_1_1_camera_render_component.html", "classmmt__gd_1_1_camera_render_component" ],
      [ "CamSmoothAutoFollowBehaviourComponent", "classmmt__gd_1_1_cam_smooth_auto_follow_behaviour_component.html", "classmmt__gd_1_1_cam_smooth_auto_follow_behaviour_component" ],
      [ "ConstantMovementComponent", "classmmt__gd_1_1_constant_movement_component.html", "classmmt__gd_1_1_constant_movement_component" ],
      [ "DebugDraw", "classmmt__gd_1_1_debug_draw.html", "classmmt__gd_1_1_debug_draw" ],
      [ "Fps", "classmmt__gd_1_1_fps.html", "classmmt__gd_1_1_fps" ],
      [ "Game", "classmmt__gd_1_1_game.html", "classmmt__gd_1_1_game" ],
      [ "GameObject", "classmmt__gd_1_1_game_object.html", "classmmt__gd_1_1_game_object" ],
      [ "GameObjectManager", "classmmt__gd_1_1_game_object_manager.html", "classmmt__gd_1_1_game_object_manager" ],
      [ "GameState", "classmmt__gd_1_1_game_state.html", "classmmt__gd_1_1_game_state" ],
      [ "GameStateManager", "classmmt__gd_1_1_game_state_manager.html", "classmmt__gd_1_1_game_state_manager" ],
      [ "IComponent", "classmmt__gd_1_1_i_component.html", "classmmt__gd_1_1_i_component" ],
      [ "InputManager", "classmmt__gd_1_1_input_manager.html", "classmmt__gd_1_1_input_manager" ],
      [ "IRenderComponent", "classmmt__gd_1_1_i_render_component.html", "classmmt__gd_1_1_i_render_component" ],
      [ "MainState", "classmmt__gd_1_1_main_state.html", "classmmt__gd_1_1_main_state" ],
      [ "MenuState", "classmmt__gd_1_1_menu_state.html", "classmmt__gd_1_1_menu_state" ],
      [ "PlayerMoveComponent", "classmmt__gd_1_1_player_move_component.html", "classmmt__gd_1_1_player_move_component" ],
      [ "PlayerShootComponent", "classmmt__gd_1_1_player_shoot_component.html", "classmmt__gd_1_1_player_shoot_component" ],
      [ "RectRenderComponent", "classmmt__gd_1_1_rect_render_component.html", "classmmt__gd_1_1_rect_render_component" ],
      [ "SpriteRenderComponent", "classmmt__gd_1_1_sprite_render_component.html", "classmmt__gd_1_1_sprite_render_component" ]
    ] ],
    [ "Game", "class_game.html", "class_game" ],
    [ "TrigonometricTraits", "structthor_1_1_trigonometric_traits.html", null ]
];