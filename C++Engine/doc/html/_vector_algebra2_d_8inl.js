var _vector_algebra2_d_8inl =
[
    [ "crossProduct", "_vector_algebra2_d_8inl.html#gac844161436d00d3b28bdc455c38849ee", null ],
    [ "cwiseProduct", "_vector_algebra2_d_8inl.html#ga0e39e6e7e4f41b9da7f8f1b85c523382", null ],
    [ "cwiseQuotient", "_vector_algebra2_d_8inl.html#ga75e4af01dec078326dd343b1b59739ea", null ],
    [ "dotProduct", "_vector_algebra2_d_8inl.html#gaf03beaae17966de61c46d85ba400c60c", null ],
    [ "length", "_vector_algebra2_d_8inl.html#gac64b6398e74001b7f1089cfe21d1745b", null ],
    [ "perpendicularVector", "_vector_algebra2_d_8inl.html#ga1f1599418c1167e05d15d46bae8ee9e5", null ],
    [ "polarAngle", "_vector_algebra2_d_8inl.html#gad86f5cb2a3d9fc002216e3e4a0157494", null ],
    [ "projectedVector", "_vector_algebra2_d_8inl.html#ga789bc21d31bbeea29333d5ed6b5c0b65", null ],
    [ "rotate", "_vector_algebra2_d_8inl.html#ga0db56a3352442dfd751950b7f47e54fa", null ],
    [ "rotatedVector", "_vector_algebra2_d_8inl.html#ga3286aa6285deb51bc400752b80dc99d5", null ],
    [ "setLength", "_vector_algebra2_d_8inl.html#ga861348e5cbd8acaa464eca8f686ad787", null ],
    [ "setPolarAngle", "_vector_algebra2_d_8inl.html#ga264efb8b7d6d54aa20819710f5843903", null ],
    [ "signedAngle", "_vector_algebra2_d_8inl.html#gad2f4e4396615f6d8fc541a51053ce079", null ],
    [ "squaredLength", "_vector_algebra2_d_8inl.html#ga4684eab3debf72da3f8a4936b21f71f6", null ],
    [ "unitVector", "_vector_algebra2_d_8inl.html#gaf80f377c55ade0ac9b511de989b0876f", null ]
];