var indexSectionsWithContent =
{
  0: "abcdefgilmoprstuvw~",
  1: "bcdfgimprst",
  2: "ms",
  3: "bcdfgimprstv",
  4: "abcdegilmoprstu~",
  5: "mp",
  6: "gptw",
  7: "f",
  8: "mv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "defines",
  8: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Macros",
  8: "Modules"
};

