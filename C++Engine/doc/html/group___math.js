var group___math =
[
    [ "MathUtil::TrigonometricTraits< T >", "struct_math_util_1_1_trigonometric_traits.html", null ],
    [ "MathUtil::TrigonometricTraits< float >", "struct_math_util_1_1_trigonometric_traits_3_01float_01_4.html", [
      [ "Type", "struct_math_util_1_1_trigonometric_traits_3_01float_01_4.html#a1e7edf3f16865ff645d9a905acbff9a4", null ]
    ] ],
    [ "MathUtil::TrigonometricTraits< double >", "struct_math_util_1_1_trigonometric_traits_3_01double_01_4.html", [
      [ "Type", "struct_math_util_1_1_trigonometric_traits_3_01double_01_4.html#af55c51c2e28779162247c91d349735c5", null ]
    ] ],
    [ "MathUtil::TrigonometricTraits< long double >", "struct_math_util_1_1_trigonometric_traits_3_01long_01double_01_4.html", [
      [ "Type", "struct_math_util_1_1_trigonometric_traits_3_01long_01double_01_4.html#ab63fcbc49c06b5194d0d3b6653a3fbca", null ]
    ] ],
    [ "TrigonometricTraits", "structthor_1_1_trigonometric_traits.html", null ],
    [ "MathUtil::toDegree", "group___math.html#gab8563f07357ec117c47468786d80beeb", null ],
    [ "MathUtil::toRadian", "group___math.html#ga5c0390134ba8bda70af947d41fd13178", null ],
    [ "MathUtil::Pi", "group___math.html#ga0690eafbb3eccd86ca8e93e84278d517", null ]
];