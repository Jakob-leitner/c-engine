﻿#pragma once
#include "IObserver.hpp"
#include "LayerComponent.hpp"
#include "MainState.hpp"

namespace mmt_gd
{
class LayerComponent;
class IRenderComponent;

class RenderManager : public IObserver, std::enable_shared_from_this<RenderManager>
{
public:
    static RenderManager& getInstance();
    RenderManager(const RenderManager& rhv) = delete;
    RenderManager(RenderManager&& rhv) = delete;
    RenderManager& operator=(const RenderManager& rhv) = delete;
    RenderManager&& operator=(RenderManager&& rhv) = delete;
    void Subscribe(const std::shared_ptr<IRenderComponent*>& renderComponent);
    void            Init(sf::RenderWindow* _window);
    void            Update() override;
    sf::Color       randomColor;
    void drawCollisionShapes(std::shared_ptr<GameObject> obj);
    void render();
    IObserver*       getPointer()
    {
        return this;
    }

    std::map<std::string, std::vector<std::shared_ptr<sf::Sprite>>> layers;
    std::vector<std::shared_ptr<GameObject>>                                        objects;

    


private:
    RenderManager() = default;
    ~RenderManager()
    {
        MainState::playerRigid->Detach(this);
    }

   
    sf::RenderWindow*                      window; //Achtung ein RawPointer (noch)
};
} // namespace mmt_gd
