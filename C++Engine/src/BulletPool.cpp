#include "stdafx.h"

#include "BulletPool.hpp"

#include "ConstantMovementComponent.hpp"
#include "GameObjectManager.hpp"
#include "SpriteRenderComponent.hpp"

#include <iostream>
#include <memory>

mmt_gd::BulletPool::BulletPool(
    const size_t       size,
    const std::string& textureFile,
    sf::IntRect        textureRect,
    const std::string& layerName,
    sf::RenderWindow&  renderWindow,
    sf::FloatRect      colliderRect,
    float              mass,
    GameObjectManager& gameObjectManager) :
m_pool(size)
{
    for (auto& gameObject : m_pool)
    {
        gameObject = std::make_shared<GameObject>("Bullet_" + std::to_string(m_globalBulletIdx++));
        gameObject->setPosition(-1000, -1000);

        auto renderComp = std::make_shared<SpriteRenderComponent>(*gameObject, renderWindow, textureFile, layerName);
        renderComp->getSprite().setTextureRect(textureRect);
        gameObject->addComponent(renderComp);

        auto motion = std::make_shared<ConstantMovementComponent>(*gameObject, sf::Vector2f(100, 0));
        gameObject->addComponent(motion);

        if (!gameObject->init())
        {
            sf::err() << "Could not initialize go " << gameObject->getId() << std::endl;
        }
        gameObjectManager.addGameObject(gameObject);
        gameObject->setActive(false);
    }

    std::cout << "## pool-size: " << m_pool.size() << std::endl;
}

mmt_gd::BulletPool::~BulletPool()
{
    for (const auto& go : m_pool)
    {
        go->markForDelete();
    }
}

mmt_gd::GameObject::Ptr mmt_gd::BulletPool::get()
{
    return m_pool[m_counter++ % m_pool.size()];
}
