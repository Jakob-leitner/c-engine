#pragma once

#include "GameObjectManager.hpp"
#include "GameState.hpp"
#include "RigidBodyComponent.hpp"
#include "TileMap.hpp"

// ReSharper disable once CppInconsistentNaming
namespace std::filesystem
{
// ReSharper disable once CppInconsistentNaming
class path;
} // namespace std::filesystem

namespace mmt_gd
{

class FINALFRONTIER_API MainState final : public GameState
{
public:
    MainState(GameStateManager* gameStateManager, Game* game);

    std::shared_ptr<GameObject> createBackgroundAt(const std::string&  id,
                                                   const sf::Vector2f& position,
                                                   const std::string&  texturePath);
    void                        init() override;
    GameObject::Ptr             createPlayer(const std::string& playerId,
                                             const std::string& playerTexture,
                                             const sf::IntRect& textureRect,
                                             int                playerIdx);
    void                        exit() override;

    void update(float deltaTime) override;
    void draw() override;
    std::shared_ptr<mmt_gd::TileMap> tileMap = std::make_shared<mmt_gd::TileMap>(); //DOKU --- TileMap Member Variable
    Sprite*                             spri    = new Sprite();
    Texture*                         tex     = new Texture();
    inline static std::shared_ptr<RigidBodyComponent> playerRigid; //Radomir warum gehts nicht ohne InLine?

private:
    GameObjectManager m_gameObjectManager;
};
} // namespace mmt_gd
