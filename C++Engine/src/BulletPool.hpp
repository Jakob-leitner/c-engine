#pragma once
#include "GameObject.hpp"

#include <vector>

namespace mmt_gd
{
class GameObjectManager;

class BulletPool
{
public:
    explicit BulletPool(size_t             size,
                        const std::string& textureFile,
                        sf::IntRect        textureRect,
                        const std::string& layerName,
                        sf::RenderWindow&  renderWindow,
                        sf::FloatRect      colliderRect,
                        float              mass,
                        GameObjectManager& gameObjectManager);

    ~BulletPool();

    GameObject::Ptr get();

    std::vector<GameObject::Ptr> m_pool;

private:
    size_t            m_counter{};
    inline static int m_globalBulletIdx{0};
};
}; // namespace mmt_gd
