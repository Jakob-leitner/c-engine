#include "stdafx.h"

#include "RenderManager.hpp"

#include "DebugDraw.hpp"
#include "GameObject.hpp"
#include "IRenderComponent.hpp"
#include "LayerComponent.hpp"
#include "MainState.hpp"
#include "RigidBodyComponent.hpp"
#include "SpriteRenderComponent.hpp"

namespace mmt_gd
{
class SpriteRenderComponent;
}

void mmt_gd::RenderManager::drawCollisionShapes(std::shared_ptr<GameObject> obj)
{
    auto component = obj->getComponent<RigidBodyComponent>();
    if (component != nullptr)
    {
        component->boxCollider->m_debug_Geometry.setPosition(obj->getPosition());
        MainState::playerRigid->boxCollider->m_debug_Geometry.setFillColor(randomColor);
        window->draw(component->boxCollider->m_debug_Geometry);
    }
}

void mmt_gd::RenderManager::render()
{
    int count = 0;
    for (auto layer : layers) //map is sorted
    {
        if (count == 2) //workaround to draw objects at correct point in time (before we draw the last tileLayer)
        {
            for (auto obj : objects)
            {
                // DebugDraw::getInstance().drawCircle(sf::Vector2f(obj.getPosition().x, obj.getPosition().y), 5, sf::Color::Red);

                obj->draw();

                drawCollisionShapes(obj);
            }

        }
        for (auto sprite : layer.second)
        {
            window->draw(*sprite);
        }
        count++;
    }
    
}
void mmt_gd::RenderManager::Update()
{
   randomColor = sf::Color(rand() % 256, rand() % 256, rand() % 256, 120);
}
void mmt_gd::RenderManager::Init(sf::RenderWindow* _window)
{
    window = _window;
    randomColor = sf::Color(0,255,0,128);

}

void mmt_gd::RenderManager::Subscribe(const std::shared_ptr<IRenderComponent*>& renderComponent)
{
   
}

mmt_gd::RenderManager& mmt_gd::RenderManager::getInstance()
{
    static RenderManager s;
    return s;
}
