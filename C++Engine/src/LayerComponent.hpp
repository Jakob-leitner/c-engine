#include "IRenderComponent.hpp"
#pragma once

namespace mmt_gd
{
class LayerComponent : public IRenderComponent
{
public:
    std::vector<std::shared_ptr<sf::Sprite>> sprites;


    LayerComponent(GameObject& gameObject, sf::RenderWindow& renderWindow, std::string textureFile, std::string layerName);

    ~LayerComponent() override;

    bool init() override;

    void update(float deltaTime) override
    {
    }

    void draw() override;

    void addSprite(std::shared_ptr<sf::Sprite> sprite)
    {
        sprites.push_back(sprite);
    }

};
}
