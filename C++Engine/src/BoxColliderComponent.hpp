#pragma once
#include "IComponent.hpp"

namespace mmt_gd
{
class RigidBodyComponent;

class BoxColliderComponent : mmt_gd::IComponent
{
public:
    sf::FloatRect      rect;
    sf::RectangleShape m_debug_Geometry;
    std::shared_ptr<RigidBodyComponent> rigidBodyComponent;

    BoxColliderComponent(GameObject&                           gameObject,
                       sf::FloatRect                         rect,
                       sf::RectangleShape                    debugShape);

    ~BoxColliderComponent() override
    {
    }

    bool init() override
    {
        return true;
    }

    void update(float deltaTime) override
    {
    }

};
}
