#pragma once
#include "stdafx.h"

#include <SFML/Graphics.hpp>
#include "GameObject.hpp"
#include "tileson.hpp"

namespace mmt_gd
{
using namespace sf;
using namespace std;

using TexturePtr = shared_ptr<Texture>;
using SpritePtr = shared_ptr<Sprite>;
using GameObjectPtr = shared_ptr<GameObject>;

class TileMap
{
public:


    void loadMap(const fs::path& filename, const Vector2f& offset, RenderWindow& renderWindow);

    GameObjectPtr loadSprite(tson::Object& object) const;

    RenderWindow* m_window;
    float m_fScrollOffset{};
    float m_fScrollOffsetPixelPrecise{};
    RenderTexture m_offscreen;
    Sprite m_offscreenSprite;

    std::unordered_map<std::string, TexturePtr> m_tileSetTexture;
    const fs::path m_resourcePath{"../assets"};

    vector<vector<SpritePtr>> m_layers;
    unordered_map<string, GameObjectPtr> m_objects;
};
} // namespace mmt_gd
