#pragma once

namespace mmt_gd
{

class IObserver
{

	 public:
         virtual ~IObserver(){};
         virtual void Update(){};

};
}
