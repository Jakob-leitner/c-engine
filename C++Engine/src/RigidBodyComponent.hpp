#pragma once

#include "BoxColliderComponent.hpp"
#include "GameObject.hpp"
#include "IObserver.hpp"
#include "IRenderComponent.hpp"
#include "ISubject.hpp"

namespace mmt_gd
{

class RigidBodyComponent final : public IComponent, public std::enable_shared_from_this<RigidBodyComponent>, public ISubject 
{
public:

    void Attach(IObserver* observer) override
    {
        list_observer_.push_back(observer);
    }

    void Detach(IObserver* observer) override
    {
        remove(list_observer_.begin(), list_observer_.end(), observer);
    }

    void Notify() override
    {
        for (auto& li : list_observer_)
        {
          li->Update();
        }
    }

    //RigidBodyComponent(GameObject& gameObject, sf::Vector2f _velocity, float mass);

    RigidBodyComponent(GameObject& gameObject, float mass, float invMass, std::shared_ptr<BoxColliderComponent> collider);

    ~RigidBodyComponent() override
    {
    }

    //void addForce(sf::Vector2f force)
    //{
    //    auto length2 = [](const sf::Vector2f& vec) -> float { return vec.x * vec.x + vec.y * vec.y; }; //Radomir was ist das?
    //    auto squaredMaxSpeed = m_maxSpeed * m_maxSpeed;
    //    if (length2(m_velocity + force) < squaredMaxSpeed)
    //    {
    //        m_impulses.push_back(force);
    //    }
    //}

    bool init() override;

    void update(float deltaTime) override
    {
    }

    // Parameters used for the rigid body physics
    float m_mass;
    float m_invMass;

    sf::Vector2f m_position;

    std::shared_ptr<BoxColliderComponent> boxCollider;

    std::vector<IObserver*> list_observer_;


    std::list<sf::Vector2f> m_forces;
    std::list<sf::Vector2f> m_impulses;

    sf::Vector2f m_acceleration;
    sf::Vector2f m_velocity;
    void         subscribeToPhysicsManager();

   /* RigidBodyComponent createBody(const float mass, const sf::FloatRect& aabb, const sf::Vector2f& position)
    {
        RigidBodyComponent body{,aabb, sf::RectangleShape({aabb.width, aabb.height}), mass, mass == 0.0F ? 0.0F : 1.0F / mass, position};

        body.m_debugGeometry.setFillColor(sf::Color::Transparent);
        body.m_debugGeometry.setOutlineColor(sf::Color::Red);
        body.m_debugGeometry.setOutlineThickness(1);

        return body;
    }*/

    //
};
} // namespace mmt_gd
