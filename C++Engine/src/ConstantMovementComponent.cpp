#include "stdafx.h"

#include "ConstantMovementComponent.hpp"

#include "GameObject.hpp"

mmt_gd::ConstantMovementComponent::ConstantMovementComponent(GameObject& gameObject, const sf::Vector2f velocity) :
IComponent(gameObject),
m_velocity(velocity)
{
}

bool mmt_gd::ConstantMovementComponent::init()
{
    return true;
}

void mmt_gd::ConstantMovementComponent::update(float deltaTime)
{
    m_gameObject.move(m_velocity * deltaTime);
}
