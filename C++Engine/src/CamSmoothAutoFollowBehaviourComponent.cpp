#include "stdafx.h"

#include "CamSmoothAutoFollowBehaviourComponent.hpp"

#include "DebugDraw.hpp"


mmt_gd::CamSmoothAutoFollowBehaviourComponent::CamSmoothAutoFollowBehaviourComponent(
    GameObject&         gameObject,
    GameObject::Ptr     target,
    const sf::FloatRect bounds,
    const sf::Vector2f  viewSize,
    const float         dampening) :
IComponent(gameObject),
m_bounds(bounds),
m_innerBounds(bounds.left + viewSize.x / 2,
              bounds.top + viewSize.y / 2,
              bounds.width - viewSize.x,
              bounds.height - viewSize.y),
m_target(std::move(target)),
m_dampening(dampening),
m_viewSize(viewSize)
{
}


bool mmt_gd::CamSmoothAutoFollowBehaviourComponent::init()
{
    return true;
}

void mmt_gd::CamSmoothAutoFollowBehaviourComponent::update(const float deltaTime)
{
    if (m_target && m_target->isMarkedForDelete())
    {
        m_target = nullptr;
    }

    if (!m_target)
    {
        return;
    }

    const sf::Vector2f actualPos = m_gameObject.getPosition();
    const auto         toTarget  = m_target->getPosition() - actualPos;

    const auto nextPos = actualPos + toTarget * m_dampening * deltaTime;

    DebugDraw::getInstance().drawRectangle({m_bounds.left, m_bounds.top},
                                           {m_bounds.width, m_bounds.height},
                                           sf::Color::Blue,
                                           sf::Color::Transparent);
    DebugDraw::getInstance().drawRectangle({m_innerBounds.left, m_innerBounds.top},
                                           {m_innerBounds.width, m_innerBounds.height},
                                           sf::Color::Red,
                                           sf::Color::Transparent);

    if (m_innerBounds.contains(nextPos))
    {
        m_gameObject.setPosition(nextPos);
    }
    else
    {
        m_gameObject.setPosition(
            {std::clamp<float>(nextPos.x, m_innerBounds.left, m_innerBounds.left + m_innerBounds.width),
             std::clamp<float>(nextPos.y, m_innerBounds.top, m_innerBounds.top + m_innerBounds.height)});
    }
}
