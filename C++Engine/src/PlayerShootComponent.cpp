﻿#include "stdafx.h"

#include "PlayerShootComponent.hpp"

#include "InputManager.hpp"

#include <SFML/System.hpp>
#include <iostream>

mmt_gd::PlayerShootComponent::PlayerShootComponent(
    GameObject&         gameObject,
    const size_t        poolSize,
    sf::RenderWindow&   renderWindow,
    const std::string&  textureFile,
    const sf::IntRect   textureRect,
    const sf::FloatRect colliderRect,
    const float         bulletMass,
    const std::string&  layerName,
    GameObjectManager&  gameObjectManager) :
IComponent(gameObject),
m_pool(poolSize, textureFile, textureRect, layerName, renderWindow, colliderRect, bulletMass, gameObjectManager)
{
}


void mmt_gd::PlayerShootComponent::shoot(sf::Vector2f directionSpeed)
{
    if (m_timeSinceLastShot > 0.2F)
    {
        const auto& bullet = m_pool.get();
        bullet->setActive(true);
        bullet->setPosition(m_gameObject.getPosition() + sf::Vector2f{30.F, 0.F});
        bullet->update(0);

        //bullet->getComponent<RigidBodyComponent>()->setVelocity(directionSpeed);
        m_timeSinceLastShot = 0;
    }
}

bool mmt_gd::PlayerShootComponent::init()
{
    return true;
}

void mmt_gd::PlayerShootComponent::update(float deltaTime)
{
    m_timeSinceLastShot += deltaTime;
    if (InputManager::getInstance().isKeyDown("fire", 0))
    {
        shoot(sf::Vector2f{1000, 0});
    }
}
