#pragma once
#include "GameObject.hpp"
#include "IComponent.hpp"

namespace mmt_gd
{
class CamSmoothAutoFollowBehaviourComponent final : public IComponent
{
public:
    CamSmoothAutoFollowBehaviourComponent(GameObject&     gameObject,
                                          GameObject::Ptr target,
                                          sf::FloatRect   bounds,
                                          sf::Vector2f    viewSize,
                                          float           dampening);

    bool init() override;
    void update(float deltaTime) override;

private:
    sf::FloatRect   m_bounds;
    sf::FloatRect   m_innerBounds;
    GameObject::Ptr m_target;
    float           m_dampening;
    sf::Vector2f    m_viewSize;
};
} // namespace mmt_gd
