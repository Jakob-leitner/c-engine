#include "stdafx.h"

#include "PlayerMoveComponent.hpp"

#include "DebugDraw.hpp"
#include "GameObject.hpp"
#include "InputManager.hpp"
#include "SpriteRenderComponent.hpp"
#include "VectorAlgebra2D.h"

namespace mmt_gd
{
PlayerMoveComponent::PlayerMoveComponent(GameObject& gameObject, const int playerIndex) :
IComponent(gameObject),
m_playerIndex(playerIndex)
{
}

bool PlayerMoveComponent::init()
{
    return true;
}

void PlayerMoveComponent::update(const float deltaTime)
{
    constexpr auto speed = 2'00.0F; ///< pixels/second
    sf::Vector2f   translation{};
    if (InputManager::getInstance().isKeyDown("right", m_playerIndex))
    {
        translation.x += speed * deltaTime;
    }
    if (InputManager::getInstance().isKeyDown("left", m_playerIndex))
    {
        translation.x -= speed * deltaTime;
    }
    if (InputManager::getInstance().isKeyDown("up", m_playerIndex))
    {
        translation.y -= speed * deltaTime;
    }
    if (InputManager::getInstance().isKeyDown("down", m_playerIndex))
    {
        translation.y += speed * deltaTime;
    }

    const auto nextPos = m_gameObject.getPosition() + translation;

    DebugDraw::getInstance().drawRectangle({m_bounds.left, m_bounds.top},
                                           {m_bounds.width, m_bounds.height},
                                           sf::Color::Cyan,
                                           sf::Color::Transparent);

    if (m_bounds.left == 0.F && m_bounds.width == 0.F && m_bounds.height == 0.F && m_bounds.top == 0.F ||
        m_bounds.contains(nextPos))
    {
        m_gameObject.setPosition(nextPos);
    }
}

void PlayerMoveComponent::setBounds(const sf::FloatRect bounds)
{
    const auto spriteSize = m_gameObject.getComponent<SpriteRenderComponent>()->getSprite().getTextureRect();
    m_bounds              = bounds;
    m_bounds.width -= static_cast<float>(spriteSize.width);
    m_bounds.height -= static_cast<float>(spriteSize.height);
}
} // namespace mmt_gd
