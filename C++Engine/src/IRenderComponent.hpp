#pragma once

#include <iostream>
#include <memory>
#include <SFML/Graphics/RenderWindow.hpp>
#include "IComponent.hpp"


namespace mmt_gd
{
class IRenderComponent : public IComponent
{
public:
    using Ptr = std::shared_ptr<IRenderComponent>;
    using WeakPtr = std::weak_ptr<IRenderComponent>;

    IRenderComponent(GameObject& gameObject, sf::RenderWindow& renderWindow) :
        IComponent(gameObject),
        m_renderWindow(renderWindow)
    {
       // RenderManager::getInstance().Subscribe(std::make_shared<IRenderComponent*>(this)); //DOKU --- Subscribe to RenderManager
    }

    virtual void draw() = 0;
    int layer = 0;


    bool operator<(const IRenderComponent& str) const
    {
        return layer < str.layer;
    }

protected:
    sf::RenderWindow& m_renderWindow;
};
} // namespace mmt_gd
