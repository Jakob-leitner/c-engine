#pragma once

#include "PhysicsManager.hpp";


namespace mmt_gd
{
class RigidBodyComponent;

class PhysicsManager
{
public:
    static PhysicsManager& getInstance();
    PhysicsManager(const PhysicsManager& rhv)             = delete;
    PhysicsManager(PhysicsManager&& rhv)                  = delete;
    PhysicsManager&  operator=(const PhysicsManager& rhv) = delete;
    PhysicsManager&& operator=(PhysicsManager&& rhv)      = delete;

    void resolveCollisions() const;
    void handleCollisions();
    void findCollisions();
    bool aabbVsAabb(const sf::FloatRect& a, const sf::FloatRect& b, sf::Vector2f& normal, float& penetration);



    void update();

    
    struct Manifold
    {
        std::shared_ptr<RigidBodyComponent> m_body1{};
        std::shared_ptr<RigidBodyComponent> m_body2{};

        float        m_penetration{};
        sf::Vector2f m_normal;
    };

    std::vector<std::shared_ptr<RigidBodyComponent>> m_bodies;
    std::vector<Manifold>           m_manifolds;

private:
    PhysicsManager()  = default;
    ~PhysicsManager() = default;

};
} // namespace mmt_gd
