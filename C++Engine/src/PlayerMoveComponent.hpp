#pragma once

#include "IComponent.hpp"

#include <SFML/Graphics/Rect.hpp>

namespace mmt_gd
{
class PlayerMoveComponent final : public IComponent
{
public:
    using Ptr = std::shared_ptr<PlayerMoveComponent>;

    PlayerMoveComponent(GameObject& gameObject, int playerIndex);

    bool init() override;
    void update(float deltaTime) override;
    void setBounds(sf::FloatRect bounds);

private:
    int           m_playerIndex;
    sf::FloatRect m_bounds{};
};
} // namespace mmt_gd
