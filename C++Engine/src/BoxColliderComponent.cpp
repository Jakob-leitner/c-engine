#include "stdafx.h"
#include "BoxColliderComponent.hpp"

mmt_gd::BoxColliderComponent::BoxColliderComponent(GameObject& gameObject, sf::FloatRect rect, sf::RectangleShape debugShape) : m_debug_Geometry(debugShape),  rect(rect),
IComponent(gameObject)
{
    m_debug_Geometry.setFillColor(sf::Color::Transparent);
    m_debug_Geometry.setOutlineColor(sf::Color::Green);
    m_debug_Geometry.setOutlineThickness(1);
}
