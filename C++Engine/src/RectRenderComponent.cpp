#include "stdafx.h"

#include "RectRenderComponent.hpp"

mmt_gd::RectRenderComponent::RectRenderComponent(
    GameObject&         gameObject,
    sf::RenderWindow&   renderWindow,
    const sf::Vector2f& size,
    const sf::Color&    outlineColor,
    const sf::Color&    fillColor) :
IRenderComponent(gameObject, renderWindow),
m_size(size),
m_outlineColor(outlineColor),
m_fillColor(fillColor)
{
}

bool mmt_gd::RectRenderComponent::init()
{
    return true;
}

void mmt_gd::RectRenderComponent::update(float deltaTime)
{
}

void mmt_gd::RectRenderComponent::draw()
{
    DebugDraw::getInstance()
        .drawRectangle(m_gameObject.getPosition() - m_gameObject.getOrigin(), m_size, m_outlineColor, m_fillColor);
}
