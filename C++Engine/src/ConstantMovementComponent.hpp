#pragma once
#include "IComponent.hpp"

#include <SFML/System/Vector2.hpp>

namespace mmt_gd
{
class ConstantMovementComponent final : public IComponent
{
public:
    explicit ConstantMovementComponent(GameObject& gameObject, sf::Vector2f velocity);

    bool init() override;
    void update(float deltaTime) override;

    sf::Vector2f m_velocity;
};
} // namespace mmt_gd
