#include "stdafx.h"

#include "MainState.hpp"

#include "CamSmoothAutoFollowBehaviourComponent.hpp"
#include "CameraRenderComponent.hpp"
#include "Game.hpp"
#include "InputManager.hpp"
#include "PlayerMoveComponent.hpp"
#include "PlayerShootComponent.hpp"
#include "SpriteRenderComponent.hpp"

#include <filesystem>
#include <iostream>
#include <memory>
#include <thread>
#include "PhysicsManager.hpp"
#include "RenderManager.hpp"
#include "TileMap.hpp"
#include "TimeManager.hpp"

//RADOMIR Was ist offscreen rendering?

namespace mmt_gd
{
namespace fs = std::filesystem;

MainState::MainState(GameStateManager* gameStateManager, Game* game) : GameState(gameStateManager, game)
{
}

std::shared_ptr<GameObject> MainState::createBackgroundAt(const std::string&  id,
                                                          const sf::Vector2f& position,
                                                          const std::string&  texturePath)
{
    const auto& bg = std::make_shared<GameObject>(id);
    bg->setPosition(position);

    const auto&
        renderComp = std::make_shared<SpriteRenderComponent>(*bg, m_game->getWindow(), texturePath, "Background");
    bg->addComponent(renderComp);

    if (!bg->init())
    {
        sf::err() << "Could not initialize bg" << std::endl;
    }

    m_gameObjectManager.addGameObject(bg);
    return bg;
}

void MainState::init()
{
    const auto   resourceFolder = fs::path("..") / "assets";
    sf::Vector2f bgTextureSize{};
    mmt_gd::GameObjectManager::init();

    //BG
    {
        const auto texturePath = (resourceFolder / "bg_space_seamless.png").string();
        const auto bg          = createBackgroundAt(std::string{"BG_00"}, sf::Vector2f{0, 0}, texturePath);
        bgTextureSize          = static_cast<sf::Vector2f>(
            bg->getComponent<SpriteRenderComponent>()->getSprite().getTexture()->getSize());
        bg->setPosition(-bgTextureSize.x, -bgTextureSize.y);

        createBackgroundAt(std::string{"BG_10"}, sf::Vector2f{0, -bgTextureSize.y}, texturePath);

        createBackgroundAt(std::string{"BG_01"}, sf::Vector2f{-bgTextureSize.x, 0}, texturePath);

        createBackgroundAt(std::string{"BG_11"}, sf::Vector2f{0, 0}, texturePath);
    }

    auto bounds = sf::FloatRect{-480, -480, 960, 960};
    // Create Player

    const auto player0 = createPlayer("Player0", (resourceFolder / "Hunter1.bmp").string(), {49, 25, 23, 23}, 0);

    player0->getComponent<PlayerMoveComponent>()->setBounds(bounds);

    // Moving camera
    { //RADOMIR: Nimmst du die Geschwungenen nur zum "formatieren"? und scopen?
        const auto& camera     = std::make_shared<GameObject>("Camera");
        const auto& renderComp = std::make_shared<CameraRenderComponent>(*camera,
                                                                         m_game->getWindow(),
                                                                         m_game->getWindow().getView());

        camera->addComponent(renderComp);

        const auto& followComponent = std::make_shared<
            CamSmoothAutoFollowBehaviourComponent>(*camera, player0, bounds, m_game->getWindow().getView().getSize(), 10.F);

        camera->addComponent(followComponent);

        if (!camera->init())
        {
            sf::err() << "Could not initialize camera" << std::endl;
        }

        m_gameObjectManager.addGameObject(camera);
    tileMap->loadMap("../assets/game.tmj", -Vector2f(480,480), m_game->getWindow()); //DOKU --- Load Map

    }
}


GameObject::Ptr MainState::createPlayer(const std::string& playerId,
                                        const std::string& playerTexture,
                                        const sf::IntRect& textureRect,
                                        int                playerIdx)
{
    const auto& player = std::make_shared<GameObject>(playerId);

    const auto&
        renderComp = std::make_shared<SpriteRenderComponent>(*player, m_game->getWindow(), playerTexture, "GameObjects");

    renderComp->getSprite().setTextureRect(textureRect);
    player->addComponent(renderComp);

    const auto& moveComponent = std::make_shared<PlayerMoveComponent>(*player, playerIdx);
    player->addComponent(moveComponent);

    const auto& shootComponent = std::make_shared<
        PlayerShootComponent>(*player,
                              5,
                              m_game->getWindow(),
                              (fs::path("../") / "assets" / "ShipGun2.bmp").string(),
                              textureRect,
                              sf::FloatRect{0, 0, 10, 10},
                              0.1F,
                              "GameObjects",
                              m_gameObjectManager);
    player->addComponent(shootComponent);

    m_gameObjectManager.addGameObject(player);

    if (!player->init())
    {
        sf::err() << "Error loading player" << std::endl;
    }

    return player;
}

void MainState::update(const float deltaTime)
{
    if (InputManager::getInstance().isKeyPressed("Exit"))
    {
        m_gameStateManager->setState("MenuState");
        return;
    }
    PhysicsManager::getInstance().update();
    m_gameObjectManager.update(deltaTime);

    // APPLY FORCE TO PLAYER
    constexpr float acc = 100.0F;
    sf::Vector2f    accVec;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        accVec = {-acc, 0.0F};
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        accVec = {acc, 0.0F};
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        accVec = {0.0F, -acc};
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        accVec = {0.0F, acc};
    }
    auto length2 = [](const sf::Vector2f& vec) -> float { return vec.x * vec.x + vec.y * vec.y; }; //Radomir was ist das?

    // speed limit
    constexpr float speedLimit        = 100.0F; //Radomir was ist constexpr
    constexpr float squaredSpeedLimit = speedLimit * speedLimit;
    if (length2(playerRigid->m_velocity + accVec * TimeManager::deltaTime) < squaredSpeedLimit)
    {
        playerRigid->m_impulses.push_back(accVec);
        playerRigid->boxCollider->m_debug_Geometry.setFillColor(sf::Color(255, 0, 0, 50));
    }
}

void MainState::draw()
{
    RenderManager::getInstance().render();
}

void MainState::exit()
{
    m_gameObjectManager.shutdown();
}
} // namespace mmt_gd
