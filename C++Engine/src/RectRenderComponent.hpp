#pragma once
#include "DebugDraw.hpp"
#include "GameObject.hpp"
#include "IComponent.hpp"
#include "IRenderComponent.hpp"

namespace mmt_gd
{
class RectRenderComponent final : public IRenderComponent
{
public:
    RectRenderComponent(GameObject&         gameObject,
                        sf::RenderWindow&   renderWindow,
                        const sf::Vector2f& size,
                        const sf::Color&    outlineColor,
                        const sf::Color&    fillColor);

    bool init() override;
    void update(float deltaTime) override;
    void draw() override;

    sf::Vector2f m_size;
    sf::Color    m_outlineColor;
    sf::Color    m_fillColor;
};
} // namespace mmt_gd
