#include "stdafx.h"
#include "PhysicsManager.hpp";

#include "RigidBodyComponent.hpp"
#include "TimeManager.hpp"


namespace mmt_gd
{
PhysicsManager& PhysicsManager::getInstance()
{
    static PhysicsManager instance;
    return instance;
}

void PhysicsManager::update()
{
   
    handleCollisions();

    // physical simulation loop
    for (auto& body : m_bodies)
    {
        sf::Vector2f forces{};
        for (const auto& f : body->m_forces)
        {
            forces += f;
        }

        for (const auto& i : body->m_impulses)
        {
            forces += i;
        }
        body->m_impulses.clear();

        // gravity
        //forces += Vector2f(0.0f, 9.0f);

        //
        // symplectic Euler
        body->m_acceleration = forces * body->m_invMass;
        body->m_velocity += body->m_acceleration * TimeManager::deltaTime;
        //body.velocity = body.velocity * 0.99f; //< simple "friction"
        body->m_position += body->m_velocity * TimeManager::deltaTime; //macht hier deltatime �berhaupt sinn?


        body->getGameObject().setPosition(body->m_position);

        //cout << "force " << forces.x << " " << forces.y << " acc " << body.m_acceleration.x << body.m_acceleration.y
        //     << " velocity " << body.m_velocity.x << " " << body.m_velocity.y << endl;
    }

}
void PhysicsManager::findCollisions()
{
    for (auto itA = m_bodies.begin(); itA != m_bodies.end(); ++itA)
    {
        auto& body1 = *itA;
        for (auto itB = itA; itB != m_bodies.end(); ++itB)
        {
            if (itA == itB)
            {
                continue;
            }

            auto& body2 = *itB;

            // if both object don't have a mass or body is the same skip
            if (body1->m_mass == 0 && body2->m_mass == 0)
            {
                continue;
            }

            sf::Transform body1Transform;
            body1Transform.translate(body1->m_position);
            sf::Transform body2Transform;
            body2Transform.translate(body2->m_position);

            sf::Vector2f normal;
            float    penetration = NAN;
            if (aabbVsAabb(body1Transform.transformRect(body1->boxCollider->rect),
                           body2Transform.transformRect(body2->boxCollider->rect),
                           normal,
                           penetration))
            {
                Manifold manifold;
                manifold.m_body1       = body1;//achtung hier geht das?
                manifold.m_body2       = body2;
                manifold.m_normal      = normal;
                manifold.m_penetration = penetration;

                m_manifolds.push_back(manifold);

                //Trigger Events
                manifold.m_body1->Notify();
                manifold.m_body2->Notify();
            }
        }
    }
}
void PhysicsManager::resolveCollisions() const
{
    for (auto man : m_manifolds)
    {
        // TODO: implement simple collision resolution without restitution. see slides for formulas
        // TODO: add restitution to collision resolution (j)
        // HINT: pay attention to the direction of the normal and relative velocity.

        // Calculate relative velocity
        const sf::Vector2f rv = man.m_body1->m_velocity - man.m_body2->m_velocity;

        // Calculate relative velocity in terms of the normal direction (length through vector projection)
        const float velAlongNormal = rv.x * man.m_normal.x + rv.y * man.m_normal.y;

        // Do not resolve if velocities are separating
        if (velAlongNormal > 0)
        {
            return;
        }

        bool restitutionOn = false;
        {
            // Apply impulse
            sf::Vector2f impulse = velAlongNormal * man.m_normal;

                          auto test    = man.m_body1->m_mass + man.m_body2->m_mass;
            auto anteil1 = (1 / test) * man.m_body1->m_mass; //workaround 
            auto Anteil2 = (1 / test) * man.m_body2->m_mass;
            man.m_body1->m_velocity -= Anteil2 * impulse;
            if (man.m_body2->m_invMass == NULL)
            {
                continue;
            }
            man.m_body2->m_velocity += anteil1 * impulse;
        }

        // TODO: implement positional correction (see slides)

         sf::Vector2f rv1 = man.m_body2->m_velocity - man.m_body1->m_velocity;

        float alongNormal = rv1.x * man.m_normal.x + rv1.y * man.m_normal.y;

        if (alongNormal > 0)
            return;

        sf::Vector2f impulse = alongNormal * man.m_normal;

        man.m_body1->m_velocity = man.m_body1->m_velocity - 0.5f * impulse;
        man.m_body2->m_velocity = man.m_body2->m_velocity + 0.5f * impulse;

        const float percent    = 0.2f;
        const float slop       = 0.01f;
        sf::Vector2f    correction = std::max(man.m_penetration - slop, 0.0f) /
                                     (man.m_body1->m_invMass + man.m_body2->m_invMass) * percent * man.m_normal;

        man.m_body1->m_position += correction;
        man.m_body2->m_position -= correction;
        // Positional correction
        bool positionalCorrection = false;
    }
}

void PhysicsManager::handleCollisions()
{
    m_manifolds.clear();

    findCollisions();
    resolveCollisions();
}

bool PhysicsManager::aabbVsAabb(const sf::FloatRect& a, const sf::FloatRect& b, sf::Vector2f& normal, float& penetration)
{
    auto getCenter = [](const sf::FloatRect& rect) -> sf::Vector2f
    { return sf::Vector2f(rect.left, rect.top) + 0.5F * sf::Vector2f(rect.width, rect.height); };

    // TODO: implement SAT for axis aligned bounding box (AABB) collision
    // TODO determine separation normal for collision resolution, i.e., the axis
    // showing the smallest penetration.
    // TODO determine the corresponding penetration depth for the positional correction step.

    const sf::Vector2f n        = getCenter(b) - getCenter(a); // Vector from A to B
    const float    aExtentX = a.width * 0.5F;              // Calculate half extents along x axis
    const float    bExtentX = b.width * 0.5F;
    const float    xOverlap = aExtentX + bExtentX - abs(n.x); // Calculate overlap on x axis

    // SAT subscribeToPhysicsManager on x axis
    if (xOverlap > 0)
    {
        const float aExtentY = a.height * 0.5F; // Calculate half extents along y axis
        const float bExtentY = b.height * 0.5F;
        const float yOverlap = aExtentY + bExtentY - abs(n.y); // Calculate overlap on y axis

        // SAT subscribeToPhysicsManager on y axis
        if (yOverlap > 0)
        {
            // Find out which axis is axis of least penetration
            if (xOverlap < yOverlap)
            {
                // Point towards B knowing that n points from A to B
                if (n.x < 0)
                {
                    normal = sf::Vector2f(1.0F, 0.0F);
                }
                else
                {
                    normal = sf::Vector2f(-1.0F, 0.0F);
                }
                penetration = xOverlap;
                return true;
            }
            // Point towards B knowing that n points from A to B
            if (n.y < 0)
            {
                normal = sf::Vector2f(0, 1);
            }
            else
            {
                normal = sf::Vector2f(0, -1);
            }
            penetration = yOverlap;
            return true;
        }
    }
    return false;

}


} // namespace mmt_gd
