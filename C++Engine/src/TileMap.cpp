﻿#include "stdafx.h"

#include "TileMap.hpp"

#include "LayerComponent.hpp"
#include "MainState.hpp"
#include "RenderManager.hpp"
#include "RigidBodyComponent.hpp"
#include "SpriteRenderComponent.hpp"

namespace mmt_gd
{
void TileMap::loadMap(const fs::path& filename, const Vector2f& offset, RenderWindow& window)
{
    tson::Tileson t;
    const std::unique_ptr<tson::Map> map = t.parse(filename);
    m_window                             = &window;
    if (map->getStatus() == tson::ParseStatus::OK)
    {
        err() << "Load tile map with size: " << map->getSize().x << ", " << map->getSize().y
            << " and tile size: " << map->getTileSize().x << ", " << map->getTileSize().y << std::endl;

        for (auto& tileSet : map->getTilesets())
        {
            fs::path tileSetPath = tileSet.getImage().u8string();

            const auto texture = make_shared<Texture>();
            if (!texture->loadFromFile((m_resourcePath / tileSetPath).string()))
            {
                err() << "Could not load texture " << m_resourcePath / tileSetPath << endl;
            }
            m_tileSetTexture[tileSet.getName()] = texture;
        }
    }
    else
    {
        std::cout << "Parse error: " << map->getStatusMessage() << std::endl;
    }

    // go through all layers
    m_layers.resize(map->getLayers().size());

    for (int layerIdx = 0; layerIdx < static_cast<int>(map->getLayers().size()); layerIdx++)
    {
        auto layer = map->getLayers()[layerIdx];
        err() << "Load layer: " << layer.getName() << " with width: " << layer.getSize().x
            << " and height: " << layer.getSize().y << std::endl;

        const int size = layer.getSize().x * layer.getSize().y;

        // iterate over all elements/tiles in the layer
        for (int i = 0; i < size; i++)
        {

            const auto gid = layer.getData()[i];

            if (gid == 0)
            {
                // 0 means there is no tile at this position.
                continue;
            }

            // get tile set for tile and allocate the corresponding tileSet texture
            const auto* const tileSet = map->getTilesetByGid(gid);
            const Vector2i tileSize(map->getTileSize().x, map->getTileSize().y);
            Texture& texture = *m_tileSetTexture[tileSet->getName()];

            // calculate position of tile
            Vector2f position;
            position.x = i % layer.getSize().x * static_cast<float>(tileSize.x);
            position.y = i / layer.getSize().x * static_cast<float>(tileSize.y);
            position += offset;

            // number of tiles in tile set texture (horizontally)
            const int tileCountX = texture.getSize().x / tileSize.x;

            // calculate 2d idx of tile in tile set texture
            const int idx = gid - tileSet->getFirstgid();
            const int idxX = idx % tileCountX; //Hier crashed

            const int idxY = idx / tileCountX;

            // calculate source area of tile in tile set texture
            IntRect source(idxX * tileSize.x, idxY * tileSize.y, tileSize.x, tileSize.y);

            // IMPORTANT: the tiles of the map are loaded here and put into layers as defined
            // in the Tiled editor. The m_layers vector contains vectors of tile sprites,
            // i.e., one vector of tile sprites for each layer. you can assign such a vector
            // to a TileLayerComponent, which is managed by the RenderManager. The RenderManager
            // takes care of rendering TileLayerComponents in the correct order
            // (foreground, background, ...)

            // create tile (Sprite) and put it into the appropriate layer.
            auto sprite = make_shared<Sprite>();
            sprite->setTexture(texture);
            sprite->setTextureRect(source);
            sprite->setPosition(position.x, position.y);

            //m_layers[layerIdx].push_back(sprite);
           RenderManager::getInstance().layers[std::to_string(layerIdx)].push_back(sprite); //DOKU --- Get Sprites
        }

    }

    //// IMPORTANT: this is the second important loop. Here the game objects defined in the
    //// Tiled editor are loaded (object layer). These game objects can easily be converted
    //// to your GameObjects used in-game. Helper functions help you structure your code, e.g.,
    //// createEnemy, createPlayer, createTreasureChest, ...
    //// These helper functions parse the information and create the correct GameObjects and Components
    //// for the GameObjects.
    //// Ideally the GameStates do not contain any object generation code anymore. Objects are loaded
    //// from the Tiled editor file, i.e., have been placed in the Tiled editor.

    //// go through all object layers
    ///
    for (auto group : map->getLayers())
    {

        // go over all objects per layer
        for (auto object : group.getObjects())
        {

            Vector2f position(object.getPosition().x, object.getPosition().y);
            position += offset;

            // example to get a texture rectangle for a sprite
            FloatRect bounds(position.x, position.y, object.getSize().x, object.getSize().y);
            // TODO: check out game.tmj and there the content contained within <object group name="Object Layer 1">
            // there you can see the properties that you can parse, that should help you set up the sprites

            // TODO check data of object (position, type, etc.)
            // type can be used to check for type of component that needs to be created.
            // In the exercise it is a GameObject by itself, in the assignments/engine
            // think about configuring components for a GameObject.

            // TODO use data to initialize a sprite (texture rect, position, texture name, game object id...)

            if (object.getName() == "Collider") //so gehts
            {
                //mach gameobject
                //adde collider
                //subscribe zu PhysicsManager
                const auto sprite = loadSprite(object);

               /* sprite->setOrigin(Vector2f(object.getPosition().x + sprite->m_sprite.getTextureRect().width/2,
                                           object.getPosition().y + sprite->m_sprite.getTextureRect().height/2));*/
                sprite->setPosition(object.getPosition().x - 480, object.getPosition().y - 480);

                /*sprite->setPosition(Vector2f(sprite->getPosition().x + object.getPosition().x,
                                             sprite->getPosition().y + object.getPosition().y));*/

                m_objects[object.getName()] = sprite;


                const auto rigidBodyComp = std::make_shared<
                    RigidBodyComponent>(*m_objects[object.getName()],
                                        100000000,
                                        0,
                                        make_shared<BoxColliderComponent>(*m_objects[object.getName()], 
                                            FloatRect(0,0, sprite->m_sprite.getTextureRect().width, sprite->m_sprite.getTextureRect().height),RectangleShape(Vector2f(sprite->m_sprite.getTextureRect().width,
                                                                sprite->m_sprite.getTextureRect().height))));
                rigidBodyComp->m_position = sprite->getPosition();

                rigidBodyComp->subscribeToPhysicsManager();
                sprite->addComponent(rigidBodyComp);

                RenderManager::getInstance().objects.push_back(sprite);
                continue;
            }
            if (object.getType() == "Sprite")
            {
                
                // IMPORTANT: a very simple sprite GameObject is created here. for your
                // assignment use the component-based architecture (e.g., GameObject + SpriteRenderComponent)
                const auto sprite = loadSprite(object);

               sprite->setOrigin(Vector2f(sprite->m_sprite.getTextureRect().width / 2,
                                           sprite->m_sprite.getTextureRect().height / 2));
                //sprite->setPosition(-480, -480);

                sprite->setPosition(object.getPosition().x - 480, object.getPosition().y - 480);
               sprite->m_sprite.setPosition(
                   Vector2f(sprite->m_sprite.getTextureRect().width / 2, sprite->m_sprite.getTextureRect().height / 2));


               /* sprite->setPosition(Vector2f(sprite->getPosition().x + object.getPosition().x,
                                             sprite->getPosition().y + object.getPosition().y));*/

                m_objects[object.getName()] = sprite;

                const auto renderComp = std::make_shared<SpriteRenderComponent>(*m_objects[object.getName()], //DOKU --- Add RenderComponents
                    *m_window,
                    "placeholder",
                    "placeholder");

                const auto rigidBodyComp = std::make_shared<RigidBodyComponent>(
                    *m_objects[object.getName()],
                    1,
                    1,
                    make_shared<BoxColliderComponent>(*m_objects[object.getName()],
                                                      FloatRect(0,
                                                                0,
                                                                sprite->m_sprite.getTextureRect().width,
                                                                sprite->m_sprite.getTextureRect().height),
                                                      RectangleShape(Vector2f(sprite->m_sprite.getTextureRect().width,
                                                                              sprite->m_sprite.getTextureRect().height))));

                rigidBodyComp->m_position = sprite->getPosition();
                rigidBodyComp->subscribeToPhysicsManager();
                renderComp->m_sprite = sprite->m_sprite;
                sprite->addComponent(renderComp);
                sprite->addComponent(rigidBodyComp);

                RenderManager::getInstance().objects.push_back(sprite);
                if (object.getName() == "Player")
                {
                    std::cout << "ffffffffff";

                    MainState::playerRigid = rigidBodyComp;
                    MainState::playerRigid->Attach(RenderManager::getInstance().getPointer());//hier fehler
                    std::cout << "ffffffffff";

                }
            }

           

        }
    }
}

GameObjectPtr TileMap::loadSprite(tson::Object& object) const
{
    auto gameObject = make_shared<GameObject>("subscribeToPhysicsManager");

    IntRect textureRect{};
    textureRect.width = object.getSize().x;
    textureRect.height = object.getSize().y;

    // IMPORTANT: note how custom attributes (object->properties) created
    // in the Tiled editor are parsed and used to initialize data for Components and
    // GameObjects.
    // also note again: the GameObject is simplified for this example. A SpriteComponent should
    // be created instead.
    for (const auto* const property : object.getProperties().get())
    {
        auto name = property->getName();
        if (name == "Texture" && !gameObject->m_texture.loadFromFile(
                (m_resourcePath / std::any_cast<string>(property->getValue())).string()))
        {
            err() << "loadSprite: Could not load texture for sprite: "
                << m_resourcePath / std::any_cast<string>(property->getValue()) << endl;
        }
        else if (name == "TextureRectLeft")
        {
            textureRect.left = std::any_cast<int>(property->getValue());
        }
        else if (name == "TextureRectTop")
        {
            textureRect.top = std::any_cast<int>(property->getValue());
        }
      
    }

    gameObject->m_sprite.setTexture(gameObject->m_texture);
    gameObject->m_sprite.setTextureRect(textureRect);
    gameObject->m_sprite.setPosition(object.getPosition().x, object.getPosition().y);
    return gameObject;
}
};
