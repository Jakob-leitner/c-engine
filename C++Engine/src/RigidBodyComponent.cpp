#include "stdafx.h" //Radomir warum  muss im sourcefile immer als erstes der Precompiled Header sein

#include "RigidBodyComponent.hpp"

#include "GameObject.hpp"
#include "PhysicsManager.hpp"

namespace mmt_gd
{

RigidBodyComponent::RigidBodyComponent(GameObject&        gameObject,
                                       float              mass,
                                       float              invMass, std::shared_ptr<BoxColliderComponent> collider) :

m_mass(mass),
boxCollider(collider),
m_invMass(invMass),
IComponent(gameObject)
{
}

void RigidBodyComponent::subscribeToPhysicsManager()
{
    PhysicsManager::getInstance().m_bodies.push_back(shared_from_this());
}

bool RigidBodyComponent::init()
{
    return true;
}

} // namespace mmt_gd
